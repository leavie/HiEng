<!DOCTYPE html>

<html lang="en">
<head>
    <title>首頁 / Hi! English</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="frontend/css/pure-release-1.0.0/pure.css">
    <link rel="stylesheet" href="frontend/css/iframe.css">

</head>
<style type="text/css">
    body {
        background-color: #292929
    }
    .main {
        background: #fff;
    }
</style>
<body>
    <div class="main">
        <iframe name="menu" class="menu" height="52" src="components/purecss/menu.html" ></iframe>
        <iframe name="content" id="content" height="1000" src="home.php" ></iframe>
    </div>
</body>
<script type="text/javascript">
    window.name="index.html"
    var content=document.getElementById('content'); // iframe element
</script>
</html>
