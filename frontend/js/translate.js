/**
 * Created by leavie on 2017/5/21.
 */

//
function popup(element) {
    // onclick
    var type = element.querySelector('input[data-set=type]').value;
    var meaning = element.querySelector('input[data-set=meaning]').value;
    alert(type);
    alert(meaning);
}
// TODO:
// - 不重複註冊事件在output div
// - 減少傳參

function addWord(word,element,event) {
    // bug: 抓取資料的元素決定好了，但是他的子元素也都會觸發事件太多了，
    // 使用event.target.tagName == "BUTTON"

    // debug
    console.dir(element)
    console.dir(event);


    var type = element.querySelector('input[data-set=type]').value;
    var meaning = element.querySelector('input[data-set=meaning]').value;
    var time = getSqlDate();

    alert(time); // 任意子元素觸發

    if (event.target.tagName == 'BUTTON') {
        var query = "?word=" + word + "&type=" + type + "&meaning=" + meaning + "&time=" + time

        location.href = '/HiEng/controllers/edit.php' + query;
    }
}

function listenLookupResult(inputId,outputId) {
    document.getElementById(outputId).addEventListener('click',
        function(event){
            var word = document.getElementById(inputId).value.trim();
            addWord(word,this,event)
        }
    );
}


function lookupWord(inputId,outputId) {
    var word = document.getElementById(inputId).value.trim();
    if(word.length === 0) {
        updatePage('<p>沒有輸入</p>',outputId)
    } else {
        wordnik(word,outputId)
    }
}

// 動態產生一個div包含所有字典資料
function generateLookupResult(word,data) {
    var html = ''
    var handle = `addWord('${word}',this)`
    var dictArr
    if (!Array.isArray(data)) {
        dictArr = data.data.translations
        // google
    } else {
        dictArr = data;
    }
    if (dictArr === undefined || dictArr.length == 0) {
        return "<p>字典中沒有這個單字</p>"
    }
    html+= `<ul class="lookup-result-list>`
    for (var word of dictArr) {
        html +=
        `<li class="lookup-result-item" ">  <!-- "onclick="${handle}" -->
        <p class="input-field-text">
        <input data-set="type" type="search" class="input-text"
        value="${word["partOfSpeech"] || "沒有詞性"}" placeholder="type">
        </p>
        <p class="input-field-text">
        <input data-set="meaning" type="search" class="input-text"
        value="${word["text"] || word["translatedText"]}" placeholder="meaning">
        <button onclick="">add</button>
        </p>
        </li>`
    }
    html+= `</ul>`
    return html;
}

function updateLookupResult(word,data,outputId) {
    var result = "<h3>查詢結果</h3>"

    if (data === undefined || data.length === 0) {
        result += "<p>字典中沒有這個單字</p>";
    } else {
        result += generateLookupResult(word,data);
    }

    updatePage(result,outputId)
}


function updatePage(result,outputId) {
    // Update the page:
    var output = document.getElementById(outputId);
    if (output.innerHTML !== undefined) {
        output.innerHTML = result;
    } else {
        output.innerText = result;
    }
}

// 獲取google translate的資料
function google(word,outputId) {
    // debug
    console.log("xhr send to google")

    var url = 'https://translation.googleapis.com/language/translate/v2?key='
    var GoogleAPIKey = 'AIzaSyBsJtcoI1HzWhf5UW-6TtBzMc3QHRE5rWA'
    // 使用者輸入的表單

    xhr(
        url + GoogleAPIKey,
        {
            type: 'POST',
            data: {
                q: word,
                source: 'en',
                target: 'zh-TW',
                model: 'nmt'
            },
            onsuccess: function(responseText, xhr) {
                // debug
                console.log(responseText);

                // Parse the response:
                var data = JSON.parse(responseText);

                // update the page:
                updateLookupResult(word,data,outputId);
            },
            onfail: function() {
                console.log('fail')
            }
        }
    );
};

// Fetch the data from wordnik
function wordnik(word, outputId) {
    // debug
    console.log("xhr send to wordnik")

    var WordnikAPIKey = "fc1815f640baadad8580605e1eb075f2c2023a97f41c707df"
    var url = "http://api.wordnik.com/v4/word.json"
    var path = "definitions"

    xhr(
        url + '/' + word + '/' + path,
        {
            type: 'GET',
            data: {
                limit: 10,
                sourceDictionaries: 'wiktionary',
                api_key: WordnikAPIKey
            },
            onsuccess: function(responseText, xhr) {
                console.log(responseText);
                // Parse the response:
                var data = JSON.parse(responseText);
                // update the page:
                updateLookupResult(word,data,outputId);
            },
            onfail: function() {
                console.log('fail')
            }
        }
    );
};

// https://github.com/baidu-ife/ife/tree/master/2015_spring/task/task0002
//
function xhr(url, options) {
    /*{type,data,onsuccess,onfail}*/
    // your implement

    // debug
    // console.log(data)
    // console.log(onsuccess)
    // console.log(type)
    // console.log(this)

    // default
    options.type = options.type || 'GET'

    // guard
    if (!options.onsuccess)
        return;

    // local
    var xhr = new XMLHttpRequest();
    var query = '';
    var data = []

    // process
    for (var key in options.data) {
        if (options.data.hasOwnProperty(key))
            data.push(encodeURIComponent(key) + '=' + encodeURIComponent(options.data[key]));
    }
    query = data.join('&');

    switch (options.type) {
    case 'GET':
        xhr.open(options.type, url + '?' + query, true)
        xhr.send(null );
        break;
    case 'POST':
        xhr.open(options.type, url, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(query);
        break;
    }
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status < 300
            || xhr.status == 304) {
                options.onsuccess(xhr.responseText, xhr);
            }
            else
                options.onfail(xhr.responseText, xhr);
        }
    }
}

// 使用示例：
// xhr(
//     'http://localhost/sever/test.json',
//     {
//         data: {
//             name: 'simon',
//             password: '123456'
//         },
//         onsuccess: function (responseText, xhr) {
//             console.log(responseText);
//         },
//         onfail: function() {

//         }
//     }
// );

// 函式用來處理xhr回應
function handleAjaxResponse(event,onsuccess,onfail) {
    // Get the event object:
    if (typeof event == 'undefined') var event = window.event;
    // Get the event target:
    var xhr = event.target ||e.srcElement
    // 非同步的方式收到回應
    // Function to be called when the readyState changes:
    xhr.onreadystatechange = function() {
        // Check the readyState property:
        if (xhr.readyState == 4) {
            // Check the status code:
            if ((xhr.status >= 200 && xhr.status < 300)
            || (xhr.status == 304)) {
                onsuccess(xhr.responseText, xhr);
            } else {
                onfail(xhr.responseText,xhr);
            }
            // End of status IF.
        }
        // End of readyState IF.
    }
    // End of onreadystatechange function.
}
