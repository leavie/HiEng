<?php
include_once ("includes/auth_util.php"); // 管理session
?>
<?php
echo "<br> In login.php auth is:" . var_dump($g_auth);
goto_page_with_auth($g_auth);
?>
<!DOCTYPE html>
<html>
<head>
    <title>建立帳戶 / Hi! English</title>
    <link rel="stylesheet" type="text/css" href="frontend/css/account-form.css">
</head>
<body>
    <form class="login-form"  method="POST" action="/HiEng/controllers/validate.php">
        <input type="text" name="g_source_page" value="/HiEng/sign-up.php" hidden>
        <h2>建立帳戶</h2>
        <div class="name-input">
            <label for="name">名字</label>
            <input type="text" name="name" value="" placeholder="Name" autocomplete="off" required="">
        </div>
        <div class="id-input">
            <label for="email">ID</label>
            <input type="text" name="id" value="" placeholder="ID" autocomplete="off" required="">
        </div>
        <div class="password-input">
            <label for="password">密碼</label>
            <input type="password" name="password" value="" placeholder="Password" autocomplete="off" required="">
        </div>
        <div class="buttons-block"><input type="submit" value="註冊" class="btn btn-login"></div>
        <div class="error <?php echo $auth ? "hide" : "" ?>">
            <p>此id以被註冊 請重新輸入 </p>
        </div>
    </form>

</body>
<script>
    function validForm(){
        alert("Sign validing")
        console.log(document.forms.id.value)
        return /\w{10}/.test(document.forms.id.value);
    }

    function init() {
       var form = document.querySelector('form.login-form')
       if (form){
            // alert("got a form");
        }
        form.onsubmit = validForm
    }

    // window.onload = init
</script>
</html>
