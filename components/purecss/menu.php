<!DOCTYPE html>
<html>
<head>
<title>Demo Pure CSS Menus</title>
<!-- <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/menus-min.css"> -->
<link rel="stylesheet" href="/HiEng/frontend/css/pure-release-1.0.0/base.css">
<link rel="stylesheet" href="/HiEng/frontend/css/pure-release-1.0.0/menus.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="pure-menu pure-menu-horizontal pure-menu-scrollable">
        <a href="#" class="pure-menu-link pure-menu-heading">Hi! English</a>
        <ul class="pure-menu-list">
            <li class="pure-menu-item"><a href="#home" class="pure-menu-link">首頁</a></li>
            <li class="pure-menu-item"><a href="#login" class="pure-menu-link">登入</a></li>
            <li class="pure-menu-item"><a href="#logout" class="pure-menu-link">登出</a></li>
            <li class="pure-menu-item"><a href="#review" class="pure-menu-link">複習</a></li>
        </ul>
    </div>
</body>
<script type="text/javascript">
    // global
    var contentPath = '/HiEng/'; // path to organizing file
    var subfilename = '.php'

    function updateParentContent(sourceLink) {
        // debug
        // alert(sourceLink)

        if (parent && parent.content) { // refernece from javscript definite guide 6th - 14.8.2
            parent.content.src = contentPath + sourceLink.hash.split('#')[1] + subfilename;
        }
        return false;
    }

    function handleMenuLink(event) {
        if (event.target.classList[0] == 'pure-menu-link') {
            switch (event.target.hash) {
                case '': // empty string or only hash
                case '#':
                    parent.location.href = parent.location.href;
                break;
                default:
                updateParentContent(event.target);
            }
        }
        event.preventDefault();
    }

    function init() {
        window.name="menu.html";
        document.querySelector('.pure-menu-list').addEventListener('click', handleMenuLink);
    }

    window.onload = init
</script>
</html>
