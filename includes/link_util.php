<!-- // 連接資料庫-->
<?php
    function link_sql_select_db($dbname = default_dbname, $username = default_username, $password = default_password, $servername = default_servername)
    {
        $link = @new mysqli("localhost", "", "", "hieng");
        if($link->connect_error) {
           echo "<br> $link->connect_error";
           善後_link($link, null);
           return $link;
        } else {
            //$link->query("SET NAMES utf8");    // 資料庫連線採 UTF8
            mysqli_query($link,"SET NAMES utf8");// 資料庫連線採 UTF8
            return $link;
        }
    }

    function query($sql) {
        $link = link_sql_select_db();
        $result = $link->query($sql);

        if($link->error) {
           echo "<br>-----------------------------------------------";
           echo "<br>In link_util::query ";
           echo "<br> Error: ",
                "<br> mysql instruction: $sql",
                "<br> mysql server msg: $link->error";
           echo "<br>-----------------------------------------------";

           $_SESSION["auth"] = false;
           $link->close();
           die ("<br> Die when {$_SERVER['PHP_SELF']} invoke query()");
        } else {
            echo "<br>----------------Query Check:-------------------------------";
            echo "<br> mysql instruction: $sql";
            echo "<br>-----------------------------------------------";
        }
        return $result;
    }

    function 善後_link($link, $invoker_message) {
        if($link->connect_error) {
            $_SESSION["auth"] = false;
            $link->close();
            die ("<br> Die when {$_SERVER['PHP_SELF']} invoke link_sql_select_db()");
       }
    }

    function error_msg_link($link,$info) {
        echo "<br>-----------------------------------------------";
        echo "<br>In link_util::error_msg_link";
        echo "<br> Error: ",
             "<br> mysql account:
                {$info['servername']}, {$info['username']}, {$info['password']}, {$info['dbname']}",
             "<br> mysql server msg: $link->connect_error";
        echo "<br>-----------------------------------------------";
    }
?>
