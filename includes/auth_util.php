<!-- 管理session快取的使用者資訊 授權驗證 訪問路由-->
<?php
session_start();
ini_set('display_errors', 0);
?>

<?php // 管理session 快取的使用者資訊
echo "SESSION:  "; var_dump($_SESSION);

$g_id = $g_name = '尚未登入';
if (isset($_SESSION["auth"])) {
    $g_auth =  $_SESSION["auth"];
} else {
    $_SESSION["auth"] = false;
}

if (isset($_SESSION["id"])) {
    $g_id =  $_SESSION["id"];
}
if (isset($_SESSION["name"])) {
    $g_name =  $_SESSION["name"];
}
?>


<?php
// 來源頁面： 間接跳轉到這個頁面的來源頁面，如果沒有的話就是自己
$counter=0;
echo "<br>---------------ROUTER----DEBUG------";
echo "<br>In auth_util: " . "counter:", ++$counter;
echo "<br> This Page: "; var_dump($_SERVER['SCRIPT_NAME']);
echo "<br> POST Source Page:  "; var_dump($_POST['g_source_page']);

$g_source_page = $g_auth_success = $g_auth_fail = $_SERVER['SCRIPT_NAME'];
if(isset($_POST['g_source_page'])) {
    $g_source_page = $_POST['g_source_page'];
}

echo "<br> Global Source Page: " ; var_dump($g_source_page);
echo "<br>-----------END-----------";

 // router
 function auth_router() {
     global $g_source_page, $g_auth_fail, $g_auth_success;
     $selfPage = $_SERVER['SCRIPT_NAME'];
     global $counter;

        switch ($selfPage) {
             case '/HiEng/login.php':
             case '/HiEng/sign-up.php':
             case '/HiEng/controllers/validate.php':
                 $g_auth_success = '/HiEng/learning.php';
                 break;
             case '/HiEng/logout.php':
                 $g_auth_success = '/HiEng/home.php';
                 break;
             case '/HiEng/learning.php':
             case '/HiEng/review.php':
             case '/HiEng/controllers/edit.php':
             case '/HiEng/controllers/gain.php':
                 $g_auth_fail = '/HiEng/login.php';
                 break;
             default:
                 $_SESSION["auth"] = false;
                 echo "<br>----------------------------";
                 echo "<br> counter:", ++$counter;
                 echo "<br>In auth_router: ";
                 echo "<br> Local Self Page: $selfPage <br>";
                 var_dump($selfPage);
                 echo "<br>------------------------------";
                 die("<br> 404 Page not found");
                 break;
         }
     }

     function goto_page_with_auth($isValid) {
        global $g_source_page, $g_auth_success, $g_auth_fail;
        global $counter;
        $selfPage = $_SERVER['SCRIPT_NAME'];

        echo "<br>-----------------------------------------------";
        echo "<br>Before auth_router";
        echo "<br>In goto_page_with_auth: ";
        echo "<br> counter:", ++$counter;
        echo "<br> Global Source Page: $g_source_page <br>";
        echo "<br> Local Self Page: $selfPage <br>";
        echo "<br> Global Auth fail Page: $g_auth_fail <br>";
        echo "<br> Global Auth Success page Page: $g_auth_success <br>";
        echo "<br>-----------------------------------------------";

        auth_router();


        echo "<br>---------------------------";
        echo "<br>After auth_router";
        echo "<br>In goto_page_with_auth: ";
        echo "<br> counter:", ++$counter;
        echo "<br> Global Source Page: $g_source_page <br>";
        echo "<br> Local Self Page: $selfPage <br>";
        echo "<br> Global Auth fail Page: $g_auth_fail <br>";
        echo "<br> Global Auth Success page Page: $g_auth_success <br>";
        echo "<br>-----------------------------------------------";


        echo "<br>------------------------------";
        if ($selfPage != $g_auth_success && $isValid) {
                header("Location: $g_auth_success");
        } else if ($selfPage != $g_source_page && !$isValid) {
                header("Location: $g_source_page");
        } else if ($selfPage != $g_auth_fail && !$isValid) {
                header("Location: $g_auth_fail");
        } else {
            echo "<br> After goto_page_with_auth(): STAY HERE ";
        }
        echo "<br>--------------------------------";
    }
?>
