<!-- 動態html -->
<?php
function echo_list($row) { // list
    $title = '"item-title"';
    $description = '"item-description"';
    foreach ($row as $key => $value) {
        echo '<li class="item">'
        ."<p class=$title>$key</p>"
        ."<p class=$description>$value</p>"
        .'</li>';
    }
}

function echo_option($row, $option_col) { // quiz option
    // $inline_event = "checkAnswer(this)";
    $inline_event = "";
    foreach ($row as $key => $value) {
        if ($key == "$option_col") {
        echo "<p class=item>
                <input onclick='$inline_event' type=\"radio\" name=options value=$value><!--
                --><span class=text>$value</span>
            </p>";
        }
    }
}

function echo_cell($row) { // td
    $title = '"item-title"';
    $description = '"item-description"';
    echo "<tr>";
    foreach ($row as $key => $value) {
        echo "<td class=\"item\">"
        ."<p class=$description>$value</p>"
        .'</td>';
    }
    echo "</tr>";
}
?>

<?php
// @para $rsult: 單字的查詢結果
// ＠output: echo html responding to query result with insterested record in database
function echo_word($result) {
// 輸出每一列的資料
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo_list($row);
        }
    } else {
        echo "新的使用者？試著添加一些單字！";
    }
}

function echo_word_list($result, $type) {
    echo"<div class=$type-word><ul class=list>";
    echo_table($result);
    echo "</ul></div>";
}

function echo_table($result) {
    if ($result->num_rows > 0) {
        $title_row = mysqli_fetch_assoc($result);
        echo "<table>";
        echo "<tr>";
        foreach ($title_row as $key => $value) {
            echo "<th>$key</th>";
        }
        echo "</tr>";
        echo_cell($title_row);
        while ($row = mysqli_fetch_assoc($result)) {
            echo_cell($row);
        }
        echo "</table>";
    } else {
        echo "新的使用者？試著添加一些單字！";
    }
}

function echo_quiz($result, $title_col, $option_col) { // 輸出小測驗，返回作為題目的一整行單字
    if ($result->num_rows > 0) {

        $title_row = mysqli_fetch_assoc($result);

        echo "<fieldset id=quiz><legend><span>題目（單選）</span>" .$title_row[$title_col]. "</legend>";
        echo_option($title_row, $option_col); // 答案選項
        while ($row = mysqli_fetch_assoc($result)) {
            echo_option($row, $option_col); // 輸出其他選項
        }
        echo "</fieldset>";
        return $title_row;
    } else {
        echo "新的使用者？試著添加一些單字！";
    }
}
?>
