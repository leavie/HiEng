<?php
include_once ("includes/link_util.php");
include_once ("includes/constant.php");


    /**
     * link_db function
     *
     * @return $link
     * @author
     **/
    function switch_db($dbname = default_dbname, $username = default_username, $password = default_password, $servername = default_servername)
    {
        $constant = 'constant';
        $script_name = $_SERVER["SCRIPT_NAME"];
        $form =
        "<fieldset>
        <lengend><span>資料庫連線錯誤</span>請輸入資料庫帳戶資訊 （可選）</lengend>
        <form method=\"POST\" action=\"$script_name\" ?>
            <!-- severname<input type=\"text\" name=\"servername\"> -->
            username<input type=\"text\" name=\"username\" placeholder=\"{$constant('default_username')}\">
            password<input type=\"text\" name=\"password\" placeholder=\"{$constant('default_password')}\">
            dbname<input type=\"text\" name=\"dbname\">
            <input type=\"submit\" name=\"submit\" value=\"submit\">
        </form>
        </fieldset>";
        echo "==============";
        echo "<br> user input <br>";
        echo "<br>" . var_dump($servername);
        echo "<br>" .var_dump($username);
        echo "<br>" .var_dump($password);
        echo "<br>" .var_dump($dbname);
        echo "==============";

        $link = @new mysqli("$servername", "$username", "$password", "$dbname");
        // $link = link_select_db($dbname,$username,$password,$servername);
        if($link->connect_error || !$link) {
            echo $form;
            error_msg_link($link,[
            "servername"=>$servername
            ,"username"=>$username
            ,"password"=>$password
            ,"dbname"=>$dbname]);
        }
        else {
            mysqli_query($link,"SET NAMES utf8");// 資料庫連線採 UTF8
            echo "<br>成功連線指定資料庫";
            return $link;
        }

    }
    function ask_db(){
        ini_set('display_errors', 0);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $username = $_POST['username'] ? $_POST['username'] : default_username;
            $password = $_POST['password'] ? $_POST['password'] : default_password;
            $dbname = $_POST['dbname'] ? $_POST['dbname'] : default_dbname;
            $servername = $_POST['servername'] ? $_POST['servername'] : default_servername;

            switch_db($dbname, $username,$password, $servername);
        } else {
            switch_db();
        }
    }
?>

<?php
ask_db();
