<?php
include_once("includes/link_util.php"); // 連接資料庫
include_once("includes/html_util.php");
include_once ("includes/auth_util.php"); // 管理session
?>

<?php
goto_page_with_auth($g_auth);
?>

<html>
<head>
    <meta charset="utf-8">
    <title>個人card</title>
</head>
<link rel="stylesheet" type="text/css" href="frontend/css/learning.css">
<link rel="stylesheet" type="text/css" href="frontend/css/tab-ui.css">
<link rel="stylesheet" type="text/css" href="frontend/css/input.css">
<style type="text/css">
</style>
<body>
    <section class="word-lookup">
        <h1>添加新單字</h1>
        <h3>查詢單字</h3>
        <label for="text">輸入單字</label>
        <input type="text" id="lookup-word" value="" placeholder="Word" autocomplete="off" required>
        <button class="btn btn-login" type="button" id="lookup-btn">lookup</button>

        <div id="lookup-result"></div>
    </section>

    <section class="word-list">
        <h1>學習清單</h1>
        <input type="radio" name="tab" value="0" title="最近添加的單字" checked>
        <input type="radio" name="tab" value="1" title="掌握的單字">
        <input type="radio" name="tab" value="2" title="不熟的單字">

        <div id="main1" class="main-content">
            <!-- <iframe src="recent-word.php"></iframe> -->
            <?php
            echo_word_list(query("SELECT `name`, `type`, `meaning`, `example`, `progress`, `source_url` FROM `card` WHERE `user_id` = '$g_id' ORDER BY `review_last_time` DESC LIMIT 0, 10"), "recent");
            ?>
        </div>
        <div id="main2" class="main-content">
            <!-- <iframe src="trouble-word.php"></iframe> -->
            <?php
            echo_word_list(query("SELECT `name`, `type`, `meaning`, `example`, `progress`, `source_url` FROM `card` WHERE `user_id` = '$g_id' AND `progress` > 80 ORDER BY `progress` ASC"), "learned");
            ?>
        </div>
        <div id="main3" class="main-content">
            <!-- <iframe src="learned-word.php"></iframe> -->
            <?php
            echo_word_list(query("SELECT `name`, `type`, `meaning`, `example`, `progress`, `source_url` FROM `card` WHERE `user_id` = '$g_id' AND `progress` < 60 ORDER BY `progress` ASC, `review_last_time` ASC"), "trobule");
            ?>
        </div>
    </section>

<script type="text/javascript" src="frontend/js/date.js"></script>
<script type="text/javascript" src="frontend/js/translate.js"></script>
<script type="text/javascript">
    function init() {
        var inputId = 'lookup-word';
        var outputId = 'lookup-result';
        document.getElementById('lookup-btn').addEventListener("click", function(){lookupWord(inputId,outputId)});
        listenLookupResult(inputId,outputId);
    }
    window.onload = init
</script>
</body>
</html>
