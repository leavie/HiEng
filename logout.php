<!-- controllers/logout.php -->
<?php
include_once  ("includes/link_util.php"); // 連接資料庫
?>

<?php
session_start();
    mysqli_close(link_sql_select_db("HiEng"));
// remove all session variables
    session_unset();
// destroy the session
    session_destroy();

header("Location: /HiEng/home.php");
?>
