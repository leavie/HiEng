<?php
include_once  "../includes/link_util.php"; // 連接資料庫
include_once  "../includes/auth_util.php"; // 管理session
?>


<?php // 更新目標單字的分數
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // local var
    $point = 10;
    $target_word_title = $_SESSION["target_word_title"];
    $target_word_option = $_SESSION["target_word_option"];
    $select_option = $_POST["options"];


    // 加分或扣分
    if ($select_option != $target_word_option) {
        $point = -$point;
    }

    // sql
    // 更新使用者id答對的單字條目的分數
    $update_target_progress_sql  = "UPDATE `card` SET `progress` = `progress` + $point WHERE `user_id` = '$g_id' AND `name` = '$target_word_title'";

    goto_page_with_auth(query($update_target_progress_sql) === TRUE);

} else {
    echo "<br>#######";
    echo "<br>In gain: ";
    echo "<br> POST request error";
    echo "<br>#######";
}
?>
