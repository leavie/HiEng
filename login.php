<?php
 include_once ("includes/auth_util.php"); // 管理session
?>
<?php
goto_page_with_auth($g_auth);
?>
<!-- html -->
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>登入 / Hi! English</title>
    <link rel="stylesheet" type="text/css" href="frontend/css/account-form.css">
</head>

<body>

    <img class="banner" src="frontend/img/banner.png" > <!-- TODO 圖片如何置中 -->
    <form class="login-form" method="POST" action="/HiEng/controllers/validate.php">
    <input type="text" name="g_source_page" value="/HiEng/login.php">
        <h2>歡迎登入 <span class="logo">Hi! English</span></h2>
        <div class="id-input">
            <label for="email">ID</label>
            <input type="text" name="id" value="" placeholder="ID" autocomplete="off" required="">
        </div>
        <div class="password-input">
            <label for="password">密碼</label>
            <input type="password" name="password" value="" placeholder="Password" autocomplete="off" required="">
        </div>
        <div class="buttons-block"><input type="submit" value="登入" class="btn btn-login"></div>
        <div class="buttons-block"><a class="btn btn-new-acc" href="/HiEng/sign-up.php">創建帳戶</a>
            <!-- <div class="create-text">創建一個帳戶，你能夠建立自己的學習清單及學習進度</div> -->
        </div>
        <div class="forgot-password">
            <a href="/auth/forgot_password">忘記你的密碼?</a>
        </div>

        <div class="error <?= $g_auth ? "hide" : "" ?>">
            <p>尚未登入</p>
        </div>
    </form>


</div>
<footer class="exactly-app-footer"></footer>


</body></html>
<!-- END HTML -->
