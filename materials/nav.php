<?php
session_start();
?>

<?php
$id = $name = '尚未登入';
if (isset($_SESSION["id"])) {
    $id =  $_SESSION["id"];
}
if (isset($_SESSION["name"])) {
    $name =  $_SESSION["name"];
}
?>

<!DOCTYPE html>
<!-- saved from url=(0048)https://kkbruce.tw/bs3/Examples/starter-template -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>導航條 / Hi! English</title>

<!-- Bootstrap core CSS -->
<link href="./src/framework/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./src/framework/starter-template.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="~/Scripts/AssetsBS3/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="./src/framework/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<style type="text/css">
    .navbar-inverse {
        background: #2196f3;
    }
</style>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Hi! English</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php">首頁</a></li>
                    <li><a href="login.php"><?php echo $name ?></a></li>
                    <li><a href="logout.php">登出</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="./src/framework/jquery.min.js"></script> -->
    <!-- <script src="./src/framework/bootstrap.min.js"></script> -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="./src/framework/ie10-viewport-bug-workaround.js"></script> -->



</body></html>

