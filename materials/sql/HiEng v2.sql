-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生日期: 2017 年 07 月 06 日 14:58
-- 伺服器版本: 5.5.32
-- PHP 版本: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `hieng v2`
--
CREATE DATABASE IF NOT EXISTS `hieng v2` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `hieng v2`;

-- --------------------------------------------------------

--
-- 表的結構 `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- 轉存資料表中的資料 `book`
--

INSERT INTO `book` (`id`, `name`) VALUES
(1, '大學生'),
(2, '美國生活'),
(3, '易混淆');

-- --------------------------------------------------------

--
-- 表的結構 `book_has_card`
--

CREATE TABLE IF NOT EXISTS `book_has_card` (
  `book_id` int(11) NOT NULL,
  `card_name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`book_id`,`card_name`),
  KEY `fk_book_has_card_card1_idx` (`card_name`),
  KEY `fk_book_has_card_book1_idx` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 轉存資料表中的資料 `book_has_card`
--

INSERT INTO `book_has_card` (`book_id`, `card_name`) VALUES
(1, 'apple'),
(2, 'apple'),
(3, 'tangible'),
(3, 'tangle'),
(3, 'tender');

-- --------------------------------------------------------

--
-- 表的結構 `card`
--

CREATE TABLE IF NOT EXISTS `card` (
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` enum('名詞','動詞','形容詞') COLLATE utf8_unicode_ci DEFAULT NULL,
  `meaning` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `example` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_url` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `progress` int(1) NOT NULL DEFAULT '20',
  `review_last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `card_UNIQUE` (`name`,`user_id`),
  KEY `fk_card_user_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 轉存資料表中的資料 `card`
--

INSERT INTO `card` (`name`, `user_id`, `type`, `meaning`, `example`, `source_url`, `progress`, `review_last_time`) VALUES
('abandon', 1, '動詞', '放棄', '', '', 25, '2017-06-14 15:06:22'),
('apple', 1, '名詞', '蘋果', '', '', 25, '2017-07-05 04:14:16'),
('apple', 2, '', 'A deciduous Eurasian tree (Malus pumila) having alternate simple leaves and white or pink flowers.', '', '', 20, '2017-06-14 16:58:02'),
('boo', 1, '', 'A loud exclamation intended to scare someone, especially a child. Usually used when one has been hidden from the victim and then suddenly appeared unexpectedly.', '', '', 20, '2017-07-05 03:49:48'),
('book', 1, '名詞', '書', '', '', 35, '2017-06-14 14:53:10'),
('bus', 1, '名詞', '公車', '', '', 40, '2017-07-05 04:12:59'),
('ethic', 2, '', 'A set of principles of right conduct.', '', '', 20, '2017-06-14 16:51:14'),
('hello', 1, '名詞', '哈囉', '', '', 60, '2017-06-14 14:40:36'),
('invincible', 2, '', 'Incapable of being overcome or defeated; unconquerable.', '', '', 20, '2017-06-14 16:46:20'),
('liberal', 1, '名詞', 'a supporter of the Liberal Party of the UK, Canada, or Australia', 'With their big win, the Liberals affirmed the durability of their claim to the unofficial tagline “Canada’s natural governing party,” after several years in which their survival was very much in doubt.自由黨獲得了巨大的勝利，肯定了他們對非官方標語“加拿大自然政黨”的聲明的持久性。', 'http://www.newyorker.com/news/news-desk/what-justin-trudeaus-victory-means-for-canada', 89, '2017-06-14 09:43:40'),
('liberal', 2, '形容詞', 'accepting different opinions and ways of behaving and tending to be sympathetic to other people', '', '', 83, '2017-06-14 09:42:51'),
('literature', 2, '', 'The body of written works of a language, period, or culture.', '', '', 20, '2017-06-14 17:03:03'),
('orange', 1, '名詞', '橘子', '', '', 35, '2017-07-05 04:11:19'),
('super', 1, '形容詞', '超級', '', '', 25, '2017-06-15 01:10:46'),
('super', 2, '', 'Informal   An article or a product of superior size, quality, or grade.', '', '', 20, '2017-06-14 16:58:13'),
('tangible', 2, '', 'Discernible by the touch; palpable:  a tangible roughness of the skin. ', '', '', 20, '2017-06-14 17:04:12'),
('tangle', 2, '', 'To mix together or intertwine in a confused mass; snarl.', '', '', 20, '2017-06-14 17:03:50'),
('tender', 2, '', 'Easily crushed or bruised; fragile:  a tender petal. ', '', '', 20, '2017-06-14 17:03:57');

-- --------------------------------------------------------

--
-- 表的結構 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL DEFAULT '0',
  `password` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `login_last_date` date NOT NULL,
  `login_last_time` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 轉存資料表中的資料 `user`
--

INSERT INTO `user` (`id`, `password`, `name`, `login_last_date`, `login_last_time`) VALUES
(0, 0, 'root', '0000-00-00', '00:00:00'),
(1, 111, '菜英文', '2014-00-00', '00:00:00'),
(2, 222, '歐巴馬', '0000-00-00', '00:00:00'),
(3, 333, '邢一善', '0000-00-00', '00:00:00'),
(4, 444, '吳傑超', '0000-00-00', '00:00:00'),
(9, 999, '李萬機', '0000-00-00', '00:00:00'),
(23, 222, '打東東', '0000-00-00', '00:00:00');

-- --------------------------------------------------------

--
-- 表的結構 `user_has_book`
--

CREATE TABLE IF NOT EXISTS `user_has_book` (
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`book_id`),
  KEY `fk_user_has_book_book1_idx` (`book_id`),
  KEY `fk_user_has_book_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 轉存資料表中的資料 `user_has_book`
--

INSERT INTO `user_has_book` (`user_id`, `book_id`) VALUES
(2, 1),
(2, 2),
(2, 3);

--
-- 匯出資料表的 Constraints
--

--
-- 資料表的 Constraints `book_has_card`
--
ALTER TABLE `book_has_card`
  ADD CONSTRAINT `fk_book_has_card_book1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_book_has_card_card1` FOREIGN KEY (`card_name`) REFERENCES `card` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 資料表的 Constraints `card`
--
ALTER TABLE `card`
  ADD CONSTRAINT `fk_card_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 資料表的 Constraints `user_has_book`
--
ALTER TABLE `user_has_book`
  ADD CONSTRAINT `fk_user_has_book_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_has_book_book1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
