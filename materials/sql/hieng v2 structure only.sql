-- MySQL Script generated by MySQL Workbench
-- Sat Jul  1 18:26:21 2017
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema hieng v2
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema hieng v2
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hieng v2` DEFAULT CHARACTER SET utf8 ;
USE `hieng v2` ;

-- -----------------------------------------------------
-- Table `hieng v2`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hieng v2`.`user` (
  `id` INT(11) NOT NULL,
  `password` INT(11) NOT NULL,
  `name` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `login_last_date` DATE NOT NULL,
  `login_last_time` TIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `hieng v2`.`card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hieng v2`.`card` (
  `name` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `user_id` INT(11) NOT NULL,
  `type` ENUM('名詞', '動詞', '形容詞') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL,
  `meaning` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL,
  `example` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL,
  `source_url` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL,
  `progress` INT(1) NOT NULL DEFAULT '20',
  `review_last_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX `fk_card_user_idx` (`user_id` ASC),
  UNIQUE INDEX `card_UNIQUE` (`name` ASC, `user_id` ASC),
  CONSTRAINT `fk_card_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `hieng v2`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `hieng v2`.`book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hieng v2`.`book` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `hieng v2`.`book_has_card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hieng v2`.`book_has_card` (
  `book_id` INT(11) NOT NULL,
  `card_name` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  INDEX `fk_book_has_card_card1_idx` (`card_name` ASC),
  INDEX `fk_book_has_card_book1_idx` (`book_id` ASC),
  PRIMARY KEY (`book_id`, `card_name`),
  CONSTRAINT `fk_book_has_card_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `hieng v2`.`book` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_book_has_card_card1`
    FOREIGN KEY (`card_name`)
    REFERENCES `hieng v2`.`card` (`name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `hieng v2`.`user_has_book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hieng v2`.`user_has_book` (
  `user_id` INT(11) NOT NULL,
  `book_id` INT(11) NOT NULL,
  INDEX `fk_user_has_book_book1_idx` (`book_id` ASC),
  INDEX `fk_user_has_book_user1_idx` (`user_id` ASC),
  PRIMARY KEY (`user_id`, `book_id`),
  CONSTRAINT `fk_user_has_book_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `hieng v2`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_has_book_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `hieng v2`.`book` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
