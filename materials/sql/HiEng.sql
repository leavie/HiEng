-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- 主機: localhost:3307
-- 產生時間： 2017 年 07 月 01 日 08:44
-- 伺服器版本: 5.6.36
-- PHP 版本： 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `hieng`
--

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `password` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `login_last_date` date NOT NULL,
  `login_last_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `user`
--

INSERT INTO `user` (`id`, `password`, `name`, `login_last_date`, `login_last_time`) VALUES
(0, 0, 'root', '0000-00-00', '00:00:00'),
(1, 111, '菜英文', '2014-00-00', '00:00:00'),
(2, 222, '歐巴馬', '0000-00-00', '00:00:00'),
(3, 333, '邢一善', '0000-00-00', '00:00:00'),
(4, 444, '吳傑超', '0000-00-00', '00:00:00'),
(9, 999, '李萬機', '0000-00-00', '00:00:00'),
(23, 222, '打東東', '0000-00-00', '00:00:00'),
(45, 45, 'hehe', '0000-00-00', '00:00:00'),
(56, 333, 'keke', '0000-00-00', '00:00:00'),
(1234, 1234, 'sssssssss', '0000-00-00', '00:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `card`
--

CREATE TABLE `card` (
  `id` int(11) NOT NULL,
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('名詞','動詞','形容詞') COLLATE utf8_unicode_ci NOT NULL,
  `meaning` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `example` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `source_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `progress` int(1) NOT NULL DEFAULT '20',
  `review_last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `card`
--

INSERT INTO `card` (`id`, `name`, `type`, `meaning`, `example`, `source_url`, `progress`, `review_last_time`, `user_id`) VALUES
(1, 'abandon', '動詞', '放棄', '', '', 25, '2017-06-14 15:06:22', 1),
(2, 'apple', '名詞', '蘋果', '', '', 15, '2017-06-14 15:00:17', 1),
(3, 'book', '名詞', '書', '', '', 35, '2017-06-14 14:53:10', 1),
(4, 'bus', '名詞', '公車', '', '', 30, '2017-06-14 14:45:21', 1),
(5, 'hello', '名詞', '哈囉', '', '', 60, '2017-06-14 14:40:36', 1),
(6, 'liberal', '名詞', 'a supporter of the Liberal Party of the UK, Canada, or Australia', 'With their big win, the Liberals affirmed the durability of their claim to the unofficial tagline “Canada’s natural governing party,” after several years in which their survival was very much in doubt.自由黨獲得了巨大的勝利，肯定了他們對非官方標語“加拿大自然政黨”的聲明的持久性。', 'http://www.newyorker.com/news/news-desk/what-justin-trudeaus-victory-means-for-canada', 89, '2017-06-14 09:43:40', 1),
(7, 'orange', '名詞', '橘子', '', '', 25, '2017-06-15 01:10:43', 1),
(9, 'super', '形容詞', '超級', '', '', 25, '2017-06-15 01:10:46', 1),
(36, 'apple', '', 'A deciduous Eurasian tree (Malus pumila) having alternate simple leaves and white or pink flowers.', '', '', 20, '2017-06-14 16:58:02', 2),
(35, 'ethic', '', 'A set of principles of right conduct.', '', '', 20, '2017-06-14 16:51:14', 2),
(34, 'invincible', '', 'Incapable of being overcome or defeated; unconquerable.', '', '', 20, '2017-06-14 16:46:20', 2),
(11, 'liberal', '形容詞', 'accepting different opinions and ways of behaving and tending to be sympathetic to other people', '', '', 83, '2017-06-14 09:42:51', 2),
(38, 'literature', '', 'The body of written works of a language, period, or culture.', '', '', 20, '2017-06-14 17:03:03', 2),
(37, 'super', '', 'Informal   An article or a product of superior size, quality, or grade.', '', '', 20, '2017-06-14 16:58:13', 2),
(41, 'tangible', '', 'Discernible by the touch; palpable:  a tangible roughness of the skin. ', '', '', 20, '2017-06-14 17:04:12', 2),
(39, 'tangle', '', 'To mix together or intertwine in a confused mass; snarl.', '', '', 20, '2017-06-14 17:03:50', 2),
(40, 'tender', '', 'Easily crushed or bruised; fragile:  a tender petal. ', '', '', 20, '2017-06-14 17:03:57', 2);

-- --------------------------------------------------------

--
-- 資料表結構 `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `card_name` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `book`
--

INSERT INTO `book` (`id`, `name`, `user_id`, `card_name`) VALUES
(1, '大學生', 2, 'apple'),
(2, '美國生活', 2, 'apple'),
(3, '易混淆', 2, 'tender'),
(4, '易混淆', 2, 'tangible'),
(5, '易混淆', 2, 'tangle');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`user_id`,`name`),
  ADD UNIQUE KEY `word_id` (`id`);

--
-- 資料表索引 `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `card`
--
ALTER TABLE `card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- 使用資料表 AUTO_INCREMENT `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
