-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- 主機: localhost:3307
-- 產生時間： 2017 年 06 月 30 日 17:41
-- 伺服器版本: 5.6.36
-- PHP 版本： 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `hieng ch`
--

-- --------------------------------------------------------

--
-- 資料表結構 `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `名字` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `book`
--

INSERT INTO `book` (`id`, `名字`) VALUES
(1, '大學生'),
(2, '美國生活'),
(3, '易混淆'),
(4, '小學生');

-- --------------------------------------------------------

--
-- 資料表結構 `book_has_card`
--

CREATE TABLE `book_has_card` (
  `book_id` int(11) NOT NULL,
  `card_name` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `book_has_card`
--

INSERT INTO `book_has_card` (`book_id`, `card_name`) VALUES
(1, 'liberal'),
(1, 'literature'),
(2, 'apple'),
(2, 'invincible'),
(2, 'liberal'),
(2, 'literature'),
(3, 'tangible'),
(3, 'tangle'),
(3, 'tender'),
(4, 'abandon'),
(4, 'apple'),
(4, 'book');

-- --------------------------------------------------------

--
-- 資料表結構 `card`
--

CREATE TABLE `card` (
  `id` int(11) NOT NULL,
  `名字` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `詞性` enum('名詞','動詞','形容詞') COLLATE utf8_unicode_ci NOT NULL,
  `釋義` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `例句` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `來源網址` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `熟悉度` int(1) NOT NULL DEFAULT '20',
  `上次複習時間` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `使用者_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `card`
--

INSERT INTO `card` (`id`, `名字`, `詞性`, `釋義`, `例句`, `來源網址`, `熟悉度`, `上次複習時間`, `使用者_id`) VALUES
(1, 'abandon', '動詞', '放棄', '', '', 25, '2017-06-14 07:06:22', 1),
(2, 'apple', '名詞', '蘋果', '', '', 15, '2017-06-14 07:00:17', 1),
(36, 'apple', '名詞', 'A deciduous Eurasian tree (Malus pumila) having alternate simple leaves and white or pink flowers.', '', '', 20, '2017-06-30 20:39:31', 2),
(3, 'book', '名詞', '書', '', '', 35, '2017-06-14 06:53:10', 1),
(4, 'bus', '名詞', '公車', '', '', 30, '2017-06-14 06:45:21', 1),
(35, 'ethic', '形容詞', 'ethics [PLURAL] a set of principles that people use to decide what is right and what is wrong', '', '', 20, '2017-06-30 20:40:33', 2),
(5, 'hello', '名詞', '哈囉', '', '', 60, '2017-06-14 06:40:36', 1),
(34, 'invincible', '形容詞', 'Incapable of being overcome or defeated; unconquerable.', '', '', 20, '2017-06-30 20:39:57', 2),
(6, 'liberal', '名詞', 'a supporter of the Liberal Party of the UK, Canada, or Australia', 'With their big win, the Liberals affirmed the durability of their claim to the unofficial tagline “Canada’s natural governing party,” after several years in which their survival was very much in doubt.自由黨獲得了巨大的勝利，肯定了他們對非官方標語“加拿大自然政黨”的聲明的持久性。', 'http://www.newyorker.com/news/news-desk/what-justin-trudeaus-victory-means-for-canada', 89, '2017-06-14 01:43:40', 1),
(11, 'liberal', '形容詞', 'accepting different opinions and ways of behaving and tending to be sympathetic to other people', '', '', 83, '2017-06-14 01:42:51', 2),
(38, 'literature', '名詞', 'The body of written works of a language, period, or culture.', '', '', 20, '2017-06-30 20:40:02', 2),
(7, 'orange', '名詞', '橘子', '', '', 25, '2017-06-14 17:10:43', 1),
(9, 'super', '形容詞', '超級', '', '', 25, '2017-06-14 17:10:46', 1),
(41, 'tangible', '形容詞', 'FORMAL something that is tangible is something that you can touch', 'His hostility was almost tangible', 'http://www.macmillandictionary.com/dictionary/british/tangible', 20, '2017-06-30 20:25:09', 2),
(39, 'tangle', '動詞', 'tangle or tangle up [INTRANSITIVE/TRANSITIVE] if something tangles, or if you tangle it, its parts become twisted round each other or round something else so that they look untidy and are difficult to', '', '', 20, '2017-06-30 20:37:10', 2),
(40, 'tender', '形容詞', 'gentle in a way that shows that you care about someone or something', 'Her voice was low and tender.', 'http://www.macmillandictionary.com/dictionary/british/tender_1', 20, '2017-06-30 20:35:15', 2);

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `密碼` int(11) NOT NULL,
  `名字` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `上次登入日期` date NOT NULL,
  `上次登入時間` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `user`
--

INSERT INTO `user` (`id`, `密碼`, `名字`, `上次登入日期`, `上次登入時間`) VALUES
(0, 0, 'root', '0000-00-00', '00:00:00'),
(1, 111, '菜英文', '2014-00-00', '00:00:00'),
(2, 222, '歐巴馬', '0000-00-00', '00:00:00'),
(3, 333, '邢一善', '0000-00-00', '00:00:00'),
(4, 444, '吳傑超', '0000-00-00', '00:00:00'),
(9, 999, '李萬機', '0000-00-00', '00:00:00'),
(23, 222, '打東東', '0000-00-00', '00:00:00'),
(45, 45, 'hehe', '0000-00-00', '00:00:00'),
(56, 333, 'keke', '0000-00-00', '00:00:00'),
(1234, 1234, 'aasdf', '0000-00-00', '00:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `user_has_book`
--

CREATE TABLE `user_has_book` (
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `user_has_book`
--

INSERT INTO `user_has_book` (`user_id`, `book_id`) VALUES
(1, 2),
(1, 4),
(2, 1),
(2, 2),
(2, 3);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `book_has_card`
--
ALTER TABLE `book_has_card`
  ADD PRIMARY KEY (`card_name`,`book_id`),
  ADD KEY `fk_card_has_book_book1_idx` (`book_id`),
  ADD KEY `fk_card_has_book_card1_idx` (`card_name`);

--
-- 資料表索引 `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`名字`,`使用者_id`) USING BTREE,
  ADD UNIQUE KEY `card_id` (`id`),
  ADD KEY `fk_card_user_idx` (`使用者_id`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `user_has_book`
--
ALTER TABLE `user_has_book`
  ADD PRIMARY KEY (`book_id`,`user_id`),
  ADD KEY `fk_user_has_book_user1_idx` (`user_id`) USING BTREE,
  ADD KEY `fk_user_has_book_book1_idx` (`book_id`) USING BTREE;

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- 使用資料表 AUTO_INCREMENT `card`
--
ALTER TABLE `card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- 已匯出資料表的限制(Constraint)
--

--
-- 資料表的 Constraints `book_has_card`
--
ALTER TABLE `book_has_card`
  ADD CONSTRAINT `fk_card_has_book_book1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_card_has_book_card1` FOREIGN KEY (`card_name`) REFERENCES `card` (`名字`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `card`
--
ALTER TABLE `card`
  ADD CONSTRAINT `fk_card_user` FOREIGN KEY (`使用者_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `user_has_book`
--
ALTER TABLE `user_has_book`
  ADD CONSTRAINT `fk_book_has_user_book1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_book_has_user_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
