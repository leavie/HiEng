-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: hieng
-- ------------------------------------------------------
-- Server version	5.6.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('名詞','動詞','形容詞') COLLATE utf8_unicode_ci NOT NULL,
  `meaning` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `example` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `source_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `progress` int(1) NOT NULL DEFAULT '20',
  `review_last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`name`,`user_id`) USING BTREE,
  UNIQUE KEY `card_id` (`id`),
  KEY `fk_card_user_idx` (`user_id`),
  CONSTRAINT `fk_card_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card`
--

LOCK TABLES `card` WRITE;
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
INSERT INTO `card` VALUES (1,'abandon','動詞','放棄','','',25,'2017-06-14 07:06:22',1),(2,'apple','名詞','蘋果','','',15,'2017-06-14 07:00:17',1),(36,'apple','','A deciduous Eurasian tree (Malus pumila) having alternate simple leaves and white or pink flowers.','','',20,'2017-06-14 08:58:02',2),(3,'book','名詞','書','','',35,'2017-06-14 06:53:10',1),(4,'bus','名詞','公車','','',30,'2017-06-14 06:45:21',1),(35,'ethic','','A set of principles of right conduct.','','',20,'2017-06-14 08:51:14',2),(5,'hello','名詞','哈囉','','',60,'2017-06-14 06:40:36',1),(34,'invincible','','Incapable of being overcome or defeated; unconquerable.','','',20,'2017-06-14 08:46:20',2),(6,'liberal','名詞','a supporter of the Liberal Party of the UK, Canada, or Australia','With their big win, the Liberals affirmed the durability of their claim to the unofficial tagline “Canada’s natural governing party,” after several years in which their survival was very much in doubt.自由黨獲得了巨大的勝利，肯定了他們對非官方標語“加拿大自然政黨”的聲明的持久性。','http://www.newyorker.com/news/news-desk/what-justin-trudeaus-victory-means-for-canada',89,'2017-06-14 01:43:40',1),(11,'liberal','形容詞','accepting different opinions and ways of behaving and tending to be sympathetic to other people','','',83,'2017-06-14 01:42:51',2),(38,'literature','','The body of written works of a language, period, or culture.','','',20,'2017-06-14 09:03:03',2),(7,'orange','名詞','橘子','','',25,'2017-06-14 17:10:43',1),(9,'super','形容詞','超級','','',25,'2017-06-14 17:10:46',1),(37,'super','','Informal   An article or a product of superior size, quality, or grade.','','',20,'2017-06-14 08:58:13',2),(41,'tangible','','Discernible by the touch; palpable:  a tangible roughness of the skin. ','','',20,'2017-06-14 09:04:12',2),(39,'tangle','','To mix together or intertwine in a confused mass; snarl.','','',20,'2017-06-14 09:03:50',2),(40,'tender','','Easily crushed or bruised; fragile:  a tender petal. ','','',20,'2017-06-14 09:03:57',2);
/*!40000 ALTER TABLE `card` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-01  2:42:23
