<?php
include_once  "includes/link_util.php";
include_once  "includes/html_util.php";
include_once ("includes/auth_util.php"); // 管理session
?>

<?php
goto_page_with_auth($g_auth);
?>

<?php
// local var
$number_option = 4;

// sql
// 隨機四個不熟的單字
$random_word_option_sql = "SELECT `name`,`meaning`
                            FROM `card`
                            WHERE `user_id` = $g_id AND `progress` < 60
                            ORDER BY RAND()
                            LIMIT $number_option";
?>

<!DOCTYPE html>
<html>
<head>
	<title>複習 / Hi! English</title>
	<link rel="stylesheet" type="text/css" href="frontend/css/review.css">
</head>
<style type="text/css">
</style>
<body>
	<div id="main3" class="main-content">
	    <?php
	    echo_word_list(query(
            "SELECT `name`, `type`, `meaning`, `example`, `progress`, `source_url`
            FROM `card`
            WHERE `user_id` = '$g_id' AND `progress` < 60
            ORDER BY `progress` ASC, `review_last_time` ASC")
        , "trobule");
	    ?>
	</div>

	<!-- TODO:
	 - [x]改為post
	 - [x]對錯由伺服器判斷
	 - []伺服器隨機生成題目與問題
	  -->
	<div class="review-list">
		<form method="POST" action="controllers/gain.php">
		<div id="main">
			<?php
			$target_word_row = echo_quiz(query($random_word_option_sql), "name", "meaning");
			$_SESSION["target_word_option"] = $target_word_row["meaning"];
			$_SESSION["target_word_title"] = $target_word_row["name"];
			?>
		</div>
		<input type="submit" value="submit">
		</form>
	</div>
</body>

<script type="text/javascript">
	var quiz, options, answerOption; // DOM
	var answer, title; // String

	function generateAnswer() {
		quiz = document.getElementById('quiz');
		options = document.querySelectorAll('#main p');
		answerOption = options[0];

		title = document.querySelector('#quiz legend')
                .firstElementChild.nextSibling.textContent;

		answer = answerOption.firstElementChild.value;

		answerIndex = rand(<?php echo $number_option ?>); // 從1開始
		console.log (`answer: ${answer} is number ${answerIndex} of option`);

		quiz.insertBefore(answerOption, options[answerIndex]);
		return answer
	}

	function feedback() { // 測驗完後的獎勵與懲罰回饋

	}

    // 返回隨機整數 範圍：1...num
	function rand(num){
        return Math.floor( Math.random() * num ) + 1;
    }

	window.onload = generateAnswer;
</script>
</html>
